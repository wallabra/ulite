#include "UnrealLight.h"


Color CL_Black = {0, 0, 0};
Color CL_White = {255, 255, 255};

Texel InitializeTexel(Color baseColor) {
    Texel res = { baseColor, {0, 0, 0}, {0, 0} };
    return res;
}

Color InterpolateColor(Color* A, Color* B, unsigned short delta) {
    Color res = {
        (unsigned long)(B->r - A->r) * delta / 65535 + A->r,
        (unsigned long)(B->g - A->g) * delta / 65535 + A->g,
        (unsigned long)(B->b - A->b) * delta / 65535 + A->b
    };
    
    return res;
}

unsigned short ProcessPower(unsigned short amount) {
    amount *= 64;
    return (amount * amount / 75000 + amount * 8269 / 65536) / 64;
}

unsigned short ReceiveLight(Texel* tx, Color* lightColor, unsigned short amount) {
    unsigned short pastAmount = tx->light.High * 256 + tx->light.Low;
    amount = ProcessPower(amount);

    if (((amount + pastAmount) / 256) <= 7) {
        tx->light.High = ((amount + pastAmount) / 256);
        tx->light.Low = (amount + pastAmount) % 256;
    }
    
    else {
        tx->light.High = 7;
        tx->light.Low = 255;
    }
    
    if (pastAmount == 0)
        tx->maxLightColor = *lightColor;
    
    else if (pastAmount != amount) {
        unsigned short ratio = (amount * 65536) / pastAmount;
        
        tx->maxLightColor = InterpolateColor(&(tx->maxLightColor), lightColor, ratio);
    }

    return amount;
}

Color TexelBaseColor(Texel* tx) {   
    if (tx->light.High < 3) {    
        return InterpolateColor(&CL_Black, &tx->baseColor, ((unsigned short)(tx->light.High) * 256 + ((unsigned short)(tx->light.Low))) * 256 / 3);
    }
        
    else if (tx->light.High < 7) {
        return InterpolateColor(&tx->baseColor, &tx->maxLightColor, ((unsigned short)(tx->light.High - 2) * 256 + ((unsigned short)(tx->light.Low))) * 256 / 4);
    }
        
    else {
        return InterpolateColor(&tx->maxLightColor, &CL_White, ((unsigned short)(tx->light.Low)) * 256 + tx->light.Low);
    }
}
