#ifndef UNREALLIGHT_HEADER
#define UNREALLIGHT_HEADER


struct TexelLight {
	unsigned char High: 3;
	unsigned char Low;
};

struct Color {
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

struct Texel {
	Color baseColor;
	Color maxLightColor;
	TexelLight light;
};


extern Color CL_Black;
extern Color CL_White;


Texel InitializeTexel(Color baseColor);
Color InterpolateColor(Color* A, Color* B, unsigned short delta);
unsigned short ProcessPower(unsigned short amount);
unsigned short ReceiveLight(Texel* tx, Color* lightColor, unsigned short amount);
Color TexelBaseColor(Texel* tx);

#endif
