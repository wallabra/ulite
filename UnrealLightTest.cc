#include <cstdio>
#include <time.h>

#include "UnrealLight.h"


int main() {
    unsigned short power = 0;

    Texel txl = InitializeTexel((Color){255, 32, 4});    
    Color lightColor = {0,0,255};
    Color lightColor2 = {255,255,200};
    
    for (unsigned short j = 0; j < 800; j++) {
        clock_t time_end; time_end = clock() + CLOCKS_PER_SEC / 50;
        while (clock() < time_end);
        
        power += ReceiveLight(&txl, &lightColor, 25);
        power += ReceiveLight(&txl, &lightColor2, 6);

        printf("Added power: %i   \n", power);        
        Color res = TexelBaseColor(&txl);
        printf("Resulting color: %i,%i,%i    \n", res.r, res.g, res.b);
        printf("Resulting luminosity: %f%%    \n", ((double)(res.r) + res.g + res.b) * 100 / 3 / 255);
        
        printf("\x1B[3A");
    }
    
    printf("\x1B[4B");
    
    return 0;
}
